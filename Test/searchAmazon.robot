*** Settings ***
Documentation    Amazon Simple Test
Library          SeleniumLibrary
Resource         ${EXECDIR}/Resource/searchAmazon.robot


*** Test Cases ***
Amazon Search Test
    [Documentation]                               Amazon Search for Macbook
    [Tags]                                        Functional Test
    User Open Browser
    User Input Macbook and Click Search Button
    User Make Sure Macbook List is appear
