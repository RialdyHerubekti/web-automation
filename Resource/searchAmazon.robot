*** Settings ***
Documentation    Amazon Simple Test
Library          SeleniumLibrary
Resource         ${EXECDIR}/Resource/variableAmazon.robot


*** Keywords ***
User Open Browser
    Open Browser               ${url}    ${browser}
    Maximize Browser Window
User Input Macbook and Click Search Button
    Input Text       ${SEARCH_FIELD}     ${Macbook}
    Click Element    ${SEARCH_BUTTON}
User Make Sure Macbook List is appear
    Page Should Contain    macbook
    Sleep                  5s
    Close Browser
