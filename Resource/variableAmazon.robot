*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${browser}          gc
${url}              https://www.amazon.com/
${SEARCH_FIELD}     xpath://*[@id="twotabsearchtextbox"]
${SEARCH_BUTTON}    xpath://*[@id="nav-search-submit-button"]
${Macbook}          macbook
